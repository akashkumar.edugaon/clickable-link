package com.example.clickablelink

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.firebase.dynamiclinks.ktx.androidParameters
import com.google.firebase.dynamiclinks.ktx.dynamicLink
import com.google.firebase.dynamiclinks.ktx.dynamicLinks
import com.google.firebase.dynamiclinks.ktx.iosParameters
import com.google.firebase.ktx.Firebase

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val dynamicLink = Firebase.dynamicLinks.dynamicLink {
            link = Uri.parse("http://metzee.com/")
            domainUriPrefix = "clickablelink.page.link"
            androidParameters { }
            iosParameters("com.example.ios") { }
        }

        val dynamicLinkUri = dynamicLink.uri
        Log.e("main", "long refer " + dynamicLinkUri.toString())
        val intent= Intent()
        intent.setAction(Intent.ACTION_SEND)
        intent.putExtra(Intent.EXTRA_TEXT,dynamicLinkUri.toString() + "http://edugaonlabs.com/")
        intent.setType("text/plain")
        startActivity(intent)
    }
}
